package com.woodws.mybatis.samples.dao;


import com.woodws.mybatis.jpa.Mapper;
import com.woodws.mybatis.jpa.annotation.Parameter;
import com.woodws.mybatis.jpa.annotation.ParameterMap;
import com.woodws.mybatis.samples.entity.User;
import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

/**
 * Created by maoxiaodong on 2016/11/23.
 */
public interface FuncMapper extends Mapper<User> {
    String funcTestFunc(String param1, String param2);

    /**
     * 非MAP类型的参数不传能OUT
     * @param
     * @return
     */
    //User callTestProcedure(String id,String name);

    /**
     * MAP类型的参数需定义@ParameterMap，依序添加参数
     * @param
     * @return
     */
    @ParameterMap({
            @Parameter(name = "id", type = Integer.class, jdbcType = JdbcType.INTEGER),
            @Parameter(name = "name", jdbcType = JdbcType.VARCHAR, mode = ParameterMode.OUT)
    })
    User callTestProcedure(Map map);
}
